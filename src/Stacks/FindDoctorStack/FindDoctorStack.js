import React from 'react'

import {createStackNavigator} from '@react-navigation/stack';
import Home from '../../Screens/HomeScreen/HomeScreen';
import Screen from '../../Screens/lastscreen/Screen';
import FindDoctor from '../../Screens/FindDoctor/FindDoctor';

const Stack = createStackNavigator();
export default function FindDoctorStack() {
    return (
        // define here all Home tabs route here
        <Stack.Navigator initialRouteName="HomeScreen" screenOptions={{headerShown: false}}>
        {/* <Stack.Screen name="HomeScreen" component={Home} /> 
        <Stack.Screen name="Screen" component={Screen} />  */}
        <Stack.Screen name="FindDoctor" component={FindDoctor} />
      </Stack.Navigator>
    )
}
