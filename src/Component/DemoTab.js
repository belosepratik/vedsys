import * as React from 'react';
import { BottomNavigation, Text } from 'react-native-paper';
import adduser from "react-native-vector-icons/AntDesign"

const MusicRoute = () => <Text>Music</Text>;

// const AlbumsRoute = () => <Text>Albums</Text>;

// const RecentsRoute = () => <Text>Recents</Text>;
// const AlbumsRoutes = () => <Text>Albums</Text>;

// const RecentsRoutes = () => <Text>Recents</Text>;
const DemoTab = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'home', title: 'Home', icon: 'home' },
    // { key: 'adduser', title: 'Adduser', icon: 'home' },
    // { key: 'pluscircleo', title: 'Pluscircleo', icon: 'home' },
    // { key: 'adduser', title: 'Adduser', icon: 'home' },
    // { key: 'user', title: 'User', icon: 'home' },
  ]);

  const renderScene = BottomNavigation.SceneMap({
    home: MusicRoute,
    // adduser: AlbumsRoute,
    // pluscircleo: RecentsRoute,
    // adduser: AlbumsRoutes,
    // user: RecentsRoutes,
  });

  return (
    <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={setIndex}
      renderScene={renderScene} style={{backgroundColor:'black'}}
    />
  );
};

export default DemoTab;