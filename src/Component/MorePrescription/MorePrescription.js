import React, {useState, createRef} from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import Cross from 'react-native-vector-icons/AntDesign';
import Horizontal from 'react-native-vector-icons/MaterialIcons';
import ActionSheet from 'react-native-actions-sheet';
import {
  responsiveFontSize as rf,
  responsiveHeight as rh,
  responsiveWidth as rw,
} from 'react-native-responsive-dimensions';
const actionSheetRef = createRef();

const MorePrescription = ({toggleActionSheet, setToggleActionSheet}) => {
  let actionSheet;
  const gotoPrescribeName = () => {
    setToggleActionSheet(12);
  };
  return (
    <View>
      <View
        style={{
          alignItems: 'center',
        }}>
        <Horizontal name="horizontal-rule" size={40} />
      </View>
      <View style={{padding: 20, marginTop: -15}}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={{fontSize: rf(2.5), fontWeight: 'bold'}}>More</Text>
          <TouchableOpacity>
            <Cross name="close" size={25} />
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            onPress={() => {
              gotoPrescribeName();
            }}>
            <Text
              style={{fontSize: rf(2), fontWeight: 'normal', marginTop: 10}}>
              Prescription
            </Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text
              style={{fontSize: rf(2), fontWeight: 'normal', marginTop: 18}}>
              Pill Report
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* </ActionSheet> */}
    </View>
  );
};

export default MorePrescription;
