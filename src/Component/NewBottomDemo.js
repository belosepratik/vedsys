import React from 'react';
import {Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';

export default function NewBottomDemo({navigation}) {
  return (
    <View
      style={{
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-around',
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        elevation: 2,
        alignItems: 'center',
      }}>
      <TouchableOpacity onPress={() => navigation.navigate('Screen')}>
        <Icon name="home" color="#9CD161" size={25} />
      </TouchableOpacity>
      <TouchableOpacity>
        <Icon name="adduser" color="#9CD161" size={25} />
      </TouchableOpacity>
      <TouchableOpacity>
        <Icon name="pluscircleo" color="#9CD161" size={25} />
      </TouchableOpacity>
      <TouchableOpacity>
        <Icon name="hearto" color="#9CD161" size={25} />
      </TouchableOpacity>
      <TouchableOpacity>
        <Icon name="user" color="#9CD161" size={25} />
      </TouchableOpacity>
    </View>
  );
}
