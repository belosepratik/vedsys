import Geocoder from 'react-native-geocoding';

export const GeoDecoding = async (lat, long, apikey) => {
  Geocoder.init(apikey);
  const json = await Geocoder.from([lat, long]);
  return json.results[0];
};
