import React, {createRef, useState, useEffect} from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  FlatList,
  View,
} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Header from '../../Component/Header/Header';
import NewBottomDemo from '../../Component/NewBottomDemo';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ActionSheet from 'react-native-actions-sheet';
import ViewDoctor from '../../Screens/FindDoctor/ViewDoctor';
import {useDispatch, useSelector} from 'react-redux';
import ViewAppointment from '../../Screens/FindDoctor/ViewAppointment';
import ApiCalls from '../../Api/Api';
import {ListItemBase} from 'react-native-elements/dist/list/ListItemBase';
const call = new ApiCalls();
function FindDoctor({navigation, route}) {
  const Doctor_Data = useSelector(
    reduxState => reduxState.services.doctorService,
  );

  console.log('Doctors Data from redux >>>>>>>', Doctor_Data);

  const actionSheetRef = createRef();
  const [indexData, setIndexData] = useState({});
  const [toggleActionSheet, setToggleActionSheet] = useState();
  const gotoViewDoctor = (item, index) => {
    console.log('item>>', item);
    console.log('index>>', index);
    setIndexData(item);
    setToggleActionSheet(13);
    actionSheetRef.current?.setModalVisible();
  };
  console.log('DoctorFlatList>>', indexData);
  const gotoViewAppointment = () => {
    setToggleActionSheet(14);
    actionSheetRef.current?.setModalVisible();
  };
  return (
    <SafeAreaView>
      <Header navigation={navigation} />
      <View
        style={{
          height: vh(89),
          // flex: 1,
          justifyContent: 'space-between',
          flexDirection: 'column',
        }}>
        <View style={styles.upper}>
          <Text style={{fontSize: vf(3), fontWeight: 'bold'}}>Find Doctor</Text>
          <Icon name="filter-alt" size={30} color="#000" />
        </View>

        <FlatList
          style={{margin: 15}}
          data={Doctor_Data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <View
              style={{
                flexDirection: 'row',
                margin: 0,
                marginTop: 8,
                // padding: 10,
                height: vh(15),
                borderColor: '#D5DBDB',
                borderWidth: 1,
                borderRadius: 10,
                //justifyContent: 'space-between',
              }}>
              <View
                style={{
                  height: vh(15),
                  borderRadius: 10,
                  width: vw(22),
                }}>
                <Image
                  source={{uri: item.pic1}}
                  style={{
                    height: vh(15),
                    width: vw(22),
                    borderTopLeftRadius: 10,
                    borderBottomLeftRadius: 10,
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  marginLeft: 5,
                  padding: 5,
                  justifyContent: 'space-between',
                }}>
                <Text style={{fontSize: vf(2), fontWeight: 'bold'}}>
                  {item.name}
                </Text>
                <Text style={{fontSize: vf(1.3)}}>{item.serviceType}</Text>
                <Text style={{fontSize: vf(1.3)}}>{item.address}</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    // width: vw(62),
                    paddingRight: 2,
                    // marginTop: 10,
                  }}>
                  <TouchableOpacity
                    style={styles.btn}
                    onPress={() => gotoViewDoctor(item, index)}>
                    <Text style={styles.alt1}>View Profile</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.btn1}
                    onPress={gotoViewAppointment}>
                    <Text style={styles.alt2}>Book Appointment</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
        />
      <ActionSheet ref={actionSheetRef}>
        {toggleActionSheet == 13 && (
          <ViewDoctor
            toggleState={toggleActionSheet}
            setToggleActionSheet={setToggleActionSheet}
            navigation={navigation}
            route={route}
            data={indexData}
          />
        )}
        {toggleActionSheet == 14 && (
          <ViewAppointment
            toggleState={toggleActionSheet}
            setToggleActionSheet={setToggleActionSheet}
            navigation={navigation}
            route={route}
          />
        )}
      </ActionSheet>
      </View>
      {/* <View style={{marginTop:-25}}>
          <NewBottomDemo />
        </View> */}
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    margin: 10,
    borderWidth: 2,
    borderColor: '#F3F6F6',
    borderRadius: 15,
    height: vh(15),
    justifyContent: 'center',
    overflow: 'hidden',
  },
  main: {
    fontSize: vf(2),
    fontWeight: 'bold',
  },
  alt: {fontSize: vf(1.5), marginTop: 2},
  alt1: {fontSize: vf(1.5), color: '#06112D', textAlign: 'center'},
  alt2: {
    fontSize: vf(1.5),
    color: '#fff',
    textAlign: 'center',
  },
  btn: {
    borderWidth: 1,
    borderColor: '#06112D',
    borderRadius: 10,
    padding: 2,
    width: vw(20),
    marginRight: 5,
  },
  btn1: {
    backgroundColor: '#06112D',
    borderRadius: 10,
    padding: 2,
    color: '#000',
    // width: 130,
    width: vw(38),
  },
  upper: {
    marginLeft: 5,
    marginRight: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
});
export default FindDoctor;
