import ActionSheet from 'react-native-actions-sheet';
import {RadioButton} from 'react-native-paper';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {createRef} from 'react';
import {Input} from 'react-native-elements';
import {FlatList} from 'react-native';
import {Image} from 'react-native';
import WeekDays from './WeekDays';
import Notify from './Notify';
import {
  responsiveFontSize as rf,
  responsiveHeight as rh,
  responsiveWidth as rw,
} from 'react-native-responsive-dimensions';
import Close from 'react-native-vector-icons/AntDesign';
const actionSheetRef = createRef();

const AddRoutine = () => {
  // let actionSheet;
  const [checked, setChecked] = React.useState('');

  return (
    <View style={{height: rh(80)}}>
      <ScrollView>
        <View style={{padding: 15}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text style={{fontWeight: 'bold', fontSize: rf(2)}}>
                Add Routine
              </Text>
            </View>
            <View>
              <TouchableOpacity>
                <Close name="close" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{padding: 10}}>
            <View>
              <TextInput
                style={{
                  color: '#9BA0AB',
                  borderBottomColor: '#9B9FAB',
                  borderBottomWidth: 1,
                  fontSize: rf(2),
                  width: rw(90),
                }}
                placeholder="Enter Action"
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'baseline',
              }}>
              <View>
                <Text style={{fontSize: rf(2)}}>Duration</Text>
              </View>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <TextInput
                  style={{
                    color: '#9BA0AB',
                    borderBottomColor: '#9BA0AB',
                    borderBottomWidth: 1,
                    marginLeft: 10,
                    fontSize: rf(1.8),
                    width: rw(30),
                    // onFocus={showDatepicker}
                  }}
                  placeholder="Hours"
                />
                {/* <Text style={{fontSize: rf(1.8.5)}}>End Date</Text> */}
                <TextInput
                  style={{
                    color: '#9BA0AB',
                    borderBottomColor: '#9BA0AB',
                    borderBottomWidth: 1,
                    marginLeft: 10,
                    fontSize: rf(1.8),
                    width: rw(30),
                    //onChangeText={showDatepicker}
                  }}
                  placeholder="Minutes"
                />
              </View>
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{fontSize: rf(2)}}>Choose Day:</Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                <WeekDays day="S" />
                <WeekDays day="M" />
                <WeekDays day="T" />
                <WeekDays day="W" />
                <WeekDays day="TH" />
                <WeekDays day="F" />
                <WeekDays day="SA" />
              </View>
              <View
                style={{
                  marginLeft: -5,
                  marginTop: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <RadioButton
                  value="first"
                  status={checked === 'first' ? 'checked' : 'unchecked'}
                  onPress={() => setChecked('first')}
                />
                <Text style={{paddingLeft: 5, fontSize: rf(2)}}>
                  Repeat every week
                </Text>
              </View>
              <View>
                <TextInput
                  style={{
                    color: '#9BA0AB',
                    borderBottomColor: '#9B9FAB',
                    borderBottomWidth: 1,
                    fontSize: rf(2),
                    width: rw(90),
                  }}
                  placeholder="Add Location"
                />
              </View>
              <View style={{marginTop: 20}}>
                <Text style={{fontSize: rf(2)}}>Notification:</Text>
                <View style={{flexDirection: 'row'}}>
                  <Notify title="Beeps" />
                  <View style={{paddingLeft: 16}}></View>
                  <Notify title="Bells" />
                  <View style={{paddingLeft: 16}}></View>
                  <Notify title="Digital" />
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{paddingHorizontal: 25}}></View>
                  <Notify title="Disco" />
                  <View style={{paddingLeft: 16}}></View>
                  <Notify title="None" />
                </View>
              </View>
              <View style={{marginTop: 20}}>
                <Text style={{fontSize: rf(2)}}>Your Next Routine:</Text>
                <View style={{flexDirection: 'row'}}>
                  <Notify title="Wed,Mar 17" />
                  <View style={{paddingLeft: 16}}></View>
                  <Notify title="Wed,Mar 17" />
                  {/* <View style={{paddingLeft:16}}></View> */}
                  {/* <Notify title="Wed,Mar 17" /> */}
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{}}></View>
                  <Notify title="Wed,Mar 17" />
                  <View style={{paddingLeft: 16}}></View>
                  <Notify title="Wed,Mar 17" />
                </View>
              </View>
            </View>
          </View>
          <View>
            <TouchableOpacity
              style={{
                alignSelf: 'center',
                backgroundColor: '#06112D',
                padding: 10,
                width: '30%',
                borderRadius: 25,
              }}
              // onPress={onPress}
            >
              <Text style={{textAlign: 'center', color: '#fff'}}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default AddRoutine;
