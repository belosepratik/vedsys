import React, {useState} from 'react';
import {View, StyleSheet, Button, Alert} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Header from '../../Component/Header/Header';

const AlertMsg = ({navigation}) => {
  // const createTwoButtonAlert = () =>
  //   Alert.alert('Alert Title', 'My Alert Msg', [
  //     {
  //       text: 'Cancel',
  //       onPress: () => console.log('Cancel Pressed'),
  //       style: 'cancel',
  //     },
  //     {text: 'OK', onPress: () => console.log('OK Pressed')},
  //   ]);

  // const createThreeButtonAlert = () =>
  //   Alert.alert('Alert Title', 'My Alert Msg', [
  //     {
  //       text: 'Ask me later',
  //       onPress: () => console.log('Ask me later pressed'),
  //     },
  //     {
  //       text: 'Cancel',
  //       onPress: () => console.log('Cancel Pressed'),
  //       style: 'cancel',
  //     },
  //     {text: 'OK', onPress: () => console.log('OK Pressed')},
  //   ]);

  return (
    <SafeAreaView>
      <View>
        <Header navigation={navigation} />
        {/* <Button title={"2-Button Alert"} onPress={createTwoButtonAlert} /> */}
        {/* <Button style={styles.component} title={"3-Button Alert"} onPress={createThreeButtonAlert} /> */}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  component: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 100,
  },
});

export default AlertMsg;
