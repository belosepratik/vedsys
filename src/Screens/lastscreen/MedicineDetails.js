import React from 'react';
import {
  View,
  Text,
  SectionList,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {setMedicineDetails} from '../../Redux/Slice/LoginSlice';
import {
  responsiveFontSize as rf,
  responsiveHeight as rh,
  responsiveWidth as rw,
} from 'react-native-responsive-dimensions';
const MedicineDetails = () => {
  const dispatch = useDispatch();
  const data = useSelector(reduxState => reduxState.login);
  const {
    medicineDetails = {
      medicineName: null,
      // monday: {morning: false, afternoon: false, night: false},
      // tuesday: {morning: false, afternoon: false, night: false},
      // wednesday: {morning: false, afternoon: false, night: false},
      // thursday: {morning: false, afternoon: false, night: false},
      // friday: {morning: false, afternoon: false, night: false},
      // saturday: {morning: false, afternoon: false, night: false},
      // sunday: {morning: false, afternoon: false, night: false},
      endDays: null,
      medicineMG: null,
      doctorName: null,
      startDate: null,
      endDate: null,
      expireDate: null,
      medicalName: null,
      medicalAddress: null,
      mobileNo: null,
      email: null,
      upiId: null,
      bankAccount: null,
    },
  } = data;
  // console.log(' Medicine Details>>>>', medicineDetails);
  return (
    <>
      <View
        style={{height: rh(85), padding: 15, justifyContent: 'space-between'}}>
        <Text style={{fontSize: rf(2.9), fontWeight: 'bold'}}>
          Medicine Details
        </Text>
        <Text>Medicine Name:{medicineDetails.medicineName}</Text>
        <Text>Start Date:{medicineDetails.startDate}</Text>
        <Text>End Date:{medicineDetails.endDate}</Text>
        <Text>Expire Date:{medicineDetails.expireDate}</Text>
        <Text>End Days:{medicineDetails.endDays}</Text>
        <Text>Medicine MG:{medicineDetails.medicineMG}</Text>
        <Text>Doctor Name:{medicineDetails.doctorName}</Text>
        <Text style={{fontSize: rf(2.5), fontWeight: 'bold'}}>
          Other Details
        </Text>
        <Text>Medical Name:{medicineDetails.medicalName}</Text>
        <Text>Medical Address:{medicineDetails.medicalAddress}</Text>
        <Text>Mobile No:{medicineDetails.mobileNo}</Text>
        <Text>Email:{medicineDetails.email}</Text>
        <Text>UPI ID:{medicineDetails.upiId}</Text>
        <Text>BankAccount:{medicineDetails.bankAccount}</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  // container: {
  //  flex: 1,
  //  paddingTop: 22
  // },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 18,
    // width: '100%',
    fontWeight: 'bold',
    backgroundColor: 'rgba(235,247,247,1.0)',
    //backgroundColor: '#BEBFC5',
  },
  item: {
    padding: 10,
    fontSize: 16,
    height: 44,
  },
});

export default MedicineDetails;
