import React, {createRef, useState} from 'react';
import ActionSheet from 'react-native-actions-sheet';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
  Modal,
  FlatList,
} from 'react-native';
import moment from 'moment';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Event from './Event';
import Medicine from './Medicine';
import MedicineDetails from './MedicineDetails';
import Sc from './Sc';
import Account from 'react-native-vector-icons/MaterialCommunityIcons';
import Menu from 'react-native-vector-icons/Entypo';
import Mic from 'react-native-vector-icons/Feather';
import Notifications from 'react-native-vector-icons/Ionicons';
import AppointmentDetails from '../../Screens/lastscreen/AppointmentDetails';
import Banner from './Banner/Banner';
import {useNavigation} from '@react-navigation/native';
import MedicineView from './MedicineView';
import AddMedicine from './AddMedicine';
import Actionsheet from '../../Views/Auth/Actionsheet';
import Header from '../../Component/Header/Header';
import {useSelector, useDispatch} from 'react-redux';
import DateTimePicker from '@react-native-community/datetimepicker';
import NotificationModal from '../../Component/NotificationModal';
const notifydata = [
  {
    id: 1,
    src: require('../../assets/images/oxycodone.png'),
    heading: 'Oxycodone Pill Time',
    subheading: 'Before Breakfast',
    time: '1min ago',
    // img: './Images/PlacementArea-29.png',
  },
  {
    id: 2,
    src: require('../../assets/images/xavier.png'),
    heading: 'Message from Xavier',
    subheading: 'Message description',
    time: '25mins ago',
    // img: './Images/PlacementArea-29.png',
  },
  {
    id: 3,
    src: require('../../assets/images/event.png'),
    heading: 'An Upcoming Event',
    subheading: 'Event Description',
    time: '55mins ago',
    // img: './Images/PlacementArea-29.png',
  },
  {
    id: 4,
    src: require('../../assets/images/pills.png'),
    heading: 'Vit C Pills Running Out',
    subheading: '3 out 0f 10',
    time: '3hrs ago ago',
    // img: './Images/PlacementArea-29.png',
  },
  {
    id: 5,
    src: require('../../assets/images/pills.png'),
    heading: 'Vit C Pills Running Out',
    subheading: '3 out 0f 10',
    time: '3hrs ago ago',
    // img: './Images/PlacementArea-29.png',
  },
];
function Screen({navigation, route}) {
  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [text, setText] = useState('Select Date and Time');
  const [showDate, setShowDate] = useState(null);
  const [toggleActionSheet, setToggleActionSheet] = useState();
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    const tempDate = moment(currentDate).format('MMM D, YYYY');
    console.log('tempDate', tempDate);
    setShowDate(tempDate);
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const actionSheetRef = createRef();
  const handleActionSheet = () => {
    actionSheetRef.current?.setModalVisible();
  };

  const actionSheetRef1 = createRef();
  const handleActionSheet1 = () => {
    actionSheetRef1.current?.setModalVisible();
  };

  const actionSheetRef2 = createRef();
  // const handleActionSheet2 = () => {
  //   actionSheetRef2.current?.setModalVisible();
  // };
  const actionSheetSchedule = createRef();
  const handleActionSheetSchedule = () => {
    actionSheetSchedule.current?.setModalVisible();
  };
  const [visible, setVisible] = useState(false);
  const showNotification = () => {
    setVisible(!visible);
  };

  // const data = useSelector(reduxState => reduxState.login);
  // const {medicineDetails} = data;
  // console.log('DATA>>', data);
  // console.log('Medicine-Details>>>', medicineDetails);

  const appointmentDetails = () => {
    setToggleActionSheet(18);
    actionSheetRef2.current?.setModalVisible();
  };
  return (
    <SafeAreaView>
      <Header
        navigation={navigation}
        showNotification={showNotification}
        //showNotification={() => showNotification()}
      />
      <ScrollView showsVerticalScrollIndicator style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            marginBottom: vw(1.6),
            justifyContent: 'space-between',
          }}>
          {/* <NotificationModal visibleData={visible} /> */}
          <Modal
            animationType={'fade'}
            transparent={true}
            visible={visible}
            onRequestClose={showNotification}>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#FFFFF',
                height: 300,
                width: '85%',
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff',
                marginTop: 80,
                marginLeft: 30,
                elevation: 5,
              }}>
              <View
                style={{
                  backgroundColor: 'white',
                  height: 300,
                  width: '100%',
                  borderRadius: 10,
                  //borderWidth: 1,
                }}>
                <View
                  style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    padding: 10,
                  }}>
                  <Text>Recent Notifications</Text>
                  <TouchableOpacity onPress={showNotification}>
                    <Text style={{color: '#9CD161'}}>Close</Text>
                  </TouchableOpacity>
                </View>
                <FlatList
                  style={{margin: 5}}
                  data={notifydata}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({item}) => (
                    <View
                      style={{
                        flexDirection: 'row',
                        padding: 10,
                        justifyContent: 'space-between',
                      }}>
                      <Image source={item.src} />
                      <View
                        style={{
                          margin: 5,
                          flexDirection: 'column',
                        }}>
                        <Text style={{fontSize: vf(1.9), fontWeight: '700'}}>
                          {item.heading}
                        </Text>
                        <Text style={{fontSize: vf(1.5)}}>
                          {item.subheading}
                        </Text>
                      </View>
                      <Text
                        style={{
                          color: '#9FACA5',
                          fontSize: vf(1.3),
                          marginTop: 15,
                        }}>
                        {item.time}
                      </Text>
                    </View>
                  )}
                />
              </View>
            </View>
          </Modal>

          <View style={{display: 'flex', flexDirection: 'row'}}>
            {/* <TouchableOpacity onPress={showDatepicker}>
              <Text style={styles.text}>{showDate}</Text>
            </TouchableOpacity>
            <Icon name="arrow-drop-down" size={35} /> */}

            {!showDate ? (
              <TouchableOpacity onPress={showDatepicker}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}>Select Date</Text>
                  <Icon name="arrow-drop-down" size={35} />
                </View>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity onPress={showDatepicker}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}>{showDate}</Text>
                  <Icon name="arrow-drop-down" size={35} />
                </View>
              </TouchableOpacity>
            )}
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={mode}
                is24Hour={true}
                display="default"
                onChange={onChange}
              />
            )}
          </View>
          {/* <View>
            <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
              <Account name="account" size={40} />
            </TouchableOpacity>
          </View> */}
        </View>
        <View>
          <Sc handleActionSheet1={handleActionSheet1} navigation={navigation} />
        </View>
        <View>
          <Text style={styles.text}>Upcoming Medicine</Text>
          {/* {medicineDetails.map(medicine => {
            <Medicine handleActionSheet={handleActionSheet} />;
          })} */}

          <Medicine handleActionSheet={handleActionSheet} />
          {/* <Medicine handleActionSheet={handleActionSheet} /> */}
        </View>
        <View style={{margin: 10}}>
          <Text style={styles.text}>Upcoming Appointment</Text>
          <TouchableOpacity
            onPress={appointmentDetails}
            style={{
              backgroundColor: '#FFF',
              padding: 10,
              borderRadius: 10,
              width: vw(90),
            }}>
            <Text style={{fontSize: vh(2.2)}}>View Appointment</Text>
          </TouchableOpacity>
        </View>
        <View style={{marginBottom: 90}}>
          <Banner />
        </View>
      </ScrollView>
      {/* <ActionSheet ref={actionSheetRef}>
        <MedicineView  />
      </ActionSheet> */}
      <ActionSheet ref={actionSheetRef1}>
        <AddMedicine />
      </ActionSheet>
      <ActionSheet ref={actionSheetRef2}>
        {toggleActionSheet == 18 && (
          <AppointmentDetails
            toggleState={toggleActionSheet}
            setToggleActionSheet={setToggleActionSheet}
            navigation={navigation}
            route={route}
          />
        )}
      </ActionSheet>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {padding: vh(2)},
  text: {fontSize: vh(3), marginBottom: vh(1), fontWeight: 'bold'},
});
export default Screen;
